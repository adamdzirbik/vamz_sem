package com.example.vamz_new;

import java.util.Date;
import java.util.UUID;

/**
 * Trieda reprezentuje poznamku
 */
public class Note {
    private UUID noteId;
    private String noteName;
    private String noteContent;

    private String date;

    public Note() {
        noteId = UUID.randomUUID();
        date = new Date().toString();
    }

    public Note(UUID paNoteId, String noteDate) {
        noteId = paNoteId;
        date = noteDate;
    }

    public UUID getNoteId() {
        return noteId;
    }

    public String getNoteName() {
        return noteName;
    }

    public String getNoteContent() {
        return noteContent;
    }

    public String getDate() {
        return date;
    }

    public void setNoteName(String noteName) {
        this.noteName = noteName;
    }

    public void setNoteContent(String noteContent) {
        this.noteContent = noteContent;
    }
}
