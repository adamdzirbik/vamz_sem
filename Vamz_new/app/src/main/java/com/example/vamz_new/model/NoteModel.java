package com.example.vamz_new.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.vamz_new.Note;
import com.example.vamz_new.NoteDatabase;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Singleton trieda, ktora sluzi na komunikaciu s SQLite DB
 */
public class NoteModel {

    private static NoteModel noteBase;
    private SQLiteDatabase db;

    private NoteModel(Context context) {
        db = new NoteDatabase(context).getWritableDatabase();
    }

    /**
     * Vrati pole Poznamok, ktore sa nachadzaju v DB
     * @return
     */
    public List<Note> getNotes() {
        Cursor cursor;
        cursor = db.query("notes", null, null, null, null, null, null);
        List<Note> notesArray = new ArrayList<>();
        try {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                String noteId = cursor.getString(cursor.getColumnIndex("note_id"));
                String noteName = cursor.getString(cursor.getColumnIndex("note_name"));
                String noteContent = cursor.getString(cursor.getColumnIndex("note_content"));
                String noteDate = cursor.getString(cursor.getColumnIndex("note_date"));

                Note note = new Note(UUID.fromString(noteId), noteDate);
                note.setNoteName(noteName);
                note.setNoteContent(noteContent);
                notesArray.add(note);
                cursor.moveToNext();
            }
        } finally {
            cursor.close(); // vzdy zatvorit cursor
        }
        return notesArray;
    }

    public static NoteModel getInstance(Context context) {
        if(noteBase == null) {
            noteBase = new NoteModel(context);
        }
        return noteBase;
    }

    /**
     * Vrati konkretnu poznamku na zaklade ID
     * @param noteId
     * @return
     */
    public Note getNote(UUID noteId) {
        Cursor cursor;
        cursor = db.query("notes", null, "note_id = ?", new String[]{noteId.toString()}, null, null, null);
        try {
            cursor.moveToFirst();
            String noteName = cursor.getString(cursor.getColumnIndex("note_name"));
            String noteContent = cursor.getString(cursor.getColumnIndex("note_content"));
            String noteDate = cursor.getString(cursor.getColumnIndex("note_date"));
            Note note = new Note(noteId, noteDate);
            note.setNoteName(noteName);
            note.setNoteContent(noteContent);
            return note;
        } finally {
            cursor.close(); // vzdy zatvorit cursor
        }
    }

    /**
     * Vlozi do DB poznamku
     * @param note
     */
    public void insertNote(Note note) {
        db.insert("notes", null, getContentValues(note));
        //notesArray.add(note);
    }

    /**
     * Vymaze z DB poznamku na zaklade ID
     * @param noteId
     */
    public void deleteNote(UUID noteId) {
        db.delete("notes", "note_id = ?", new String[]{noteId.toString()
        });
    }

    /**
     * Aktualizuje poznamku v DB
     * @param noteId
     * @param noteName
     * @param noteContent
     * @param noteDate
     */
    public void updateNote(UUID noteId, String noteName, String noteContent, String noteDate) {
        Note note = new Note(noteId, noteDate);
        note.setNoteName(noteName);
        note.setNoteContent(noteContent);
        db.update("notes", getContentValues(note), "note_id = ?", new String[]{noteId.toString()} );
    }

    /**
     * Rozdeli Poznamku a vytvori ContentValues, ktore potrebujem pre komunikaciu s DB
     * @param note
     * @return
     */
    private ContentValues getContentValues(Note note) {
        ContentValues cw = new ContentValues();
        cw.put("note_id", note.getNoteId().toString());
        cw.put("note_name", note.getNoteName());
        cw.put("note_content", note.getNoteContent());
        cw.put("note_date", note.getDate());
        return cw;
    }

}
