package com.example.vamz_new;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

/**
 * Trieda sa stara o vytvorenie databazy
 * Zdroj: https://developer.android.com/reference/android/database/sqlite/SQLiteOpenHelper#onUpgrade(android.database.sqlite.SQLiteDatabase,%20int,%20int)
 */
public class NoteDatabase extends SQLiteOpenHelper {
    public NoteDatabase(@Nullable Context context) {
        super(context, "notesDataBase.db", null , 1);
    }

    /**
     * Vytvorenie tabulky
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table notes(" +
                "id integer primary key autoincrement, " +
                "note_id, " +
                "note_name, " +
                "note_content," +
                "note_date)" );
    }

    /**
     * Vola sa ak potrebujeme aktualizov DB
     *  napriklad drop table, add table
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
