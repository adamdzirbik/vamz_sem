package com.example.vamz_new.controller;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vamz_new.model.RecordModel;
import com.example.vamz_new.R;
import com.example.vamz_new.Record;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Trieda sa stara o zobrazenie vytvorenych audio zaznamov
 */
public class RecordsController extends AppCompatActivity {

    private RecyclerView recordRW;
    private List<Record> records;
    private RecordAdapter recordAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_records);

        recordRW = (RecyclerView) findViewById(R.id.recordsRW);
        recordRW.setLayoutManager(new LinearLayoutManager(this));

        records = new ArrayList<Record>();

        if (!this.checkWriteExternalPermission()) {
            finish();
        }

        records = RecordModel.getInstance().getRecords();
        recordAdapter = new RecordAdapter(records);
        recordRW.setAdapter(recordAdapter);
    }

    class RecordAdapter extends RecyclerView.Adapter<RecordViewHolder> {

        private List<Record> records;

        public RecordAdapter(List<Record> paRecords) {
            super();
            records = paRecords;
        }

        /**
         * Vrati ViewHolder
         * @param parent
         * @param viewType
         * @return
         */
        @NonNull
        @Override
        public RecordViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new RecordViewHolder(parent);
        }

        /**
         * Vlozi prvky do ViewHoldera
         * @param holder
         * @param position
         */
        @Override
        public void onBindViewHolder(@NonNull RecordViewHolder holder, int position) {
            holder.bind(this.records.get(position));
        }

        /**
         * Vrati pocet prvkov
         * @return
         */
        @Override
        public int getItemCount() {
            return records.size();
        }

        /**
         * Nastavi novy zoznam prvkov
         * @param paRecords
         */
        public void setRecords(List<Record> paRecords) {
            records = paRecords;
        }
    }

    class RecordViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView name;
        private ImageButton play, remove;

        /**
         * Nastavi prvky do pohladu
         * A priradi im onClick() -> prehraj zvuk
         * @param container
         */
        public RecordViewHolder(ViewGroup container) {

            super(LayoutInflater.from(RecordsController.this).inflate(R.layout.record_list_item, container, false));

            name = (TextView) itemView.findViewById(R.id.recordName);
            play = (ImageButton) itemView.findViewById(R.id.playButton);
            remove = (ImageButton) itemView.findViewById(R.id.removeBtn);

            play.setOnClickListener(this);

        }


        /**
         * po stlaceni tlacidla v recycle view prvku sa prehra prislusna nahravka
         * @param v
         */
        @Override
        public void onClick(View v) {
            MediaPlayer mediaPlayer = new MediaPlayer();
            String file = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Records/" + name.getText() + ".3pg";
            File newFile = new File(file); // acquire the file from path string
            FileInputStream inputStream = null;
            try {
                inputStream = new FileInputStream(newFile);
            } catch (FileNotFoundException e) {
                Log.e("play sound", "onClick: something is wrong",e);

            }
            try {
                mediaPlayer.setDataSource(inputStream.getFD());
                inputStream.close();
            } catch (IOException e) {
                Log.e("play sound", "onClick: something is wrong",e);

            }
            try {
                mediaPlayer.prepare();
                mediaPlayer.start();
                Toast.makeText(getApplicationContext(), "Playing...", Toast.LENGTH_LONG).show();
            } catch ( Exception e ) {
                Log.e("play sound", "onClick: something is wrong",e);
            }
        }

        /**
         * Nastavi text do TextView objektu
         * @param record
         */
        public void bind(Record record) {
            name.setText(record.getName());
        }

    }

    /**
     * Nastavi nove menu
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.notes_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Vyberie co sa vykona po kliknuti na Menu item
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.newNote:
                Intent intent = new Intent(RecordsController.this, AudioRecordController.class);
                startActivityForResult(intent, 1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Spracuje vysledok "child" aktivit
     * Ak sa nieco zmenilo upozorni Adapter a ten aktualizuje obsah
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            this.records = RecordModel.getInstance().getRecords();
            recordAdapter.setRecords(records);
            if(requestCode == 1 ) {
                if (data != null) {
                    String action = data.getStringExtra("action");
                    if (action.equals("new_record")) {
                        recordAdapter.notifyDataSetChanged();
                    }
                }
            }
        }
    }

    /**
     * Skontroluje ci uzivatel udelil prava
     * Ak prava nie su udelene tak ich vypytat
     * @return
     */
    private boolean checkWriteExternalPermission()
    {
        String permission = Manifest.permission.READ_EXTERNAL_STORAGE;
        int res = getApplicationContext().checkCallingOrSelfPermission(permission);
        Log.i("this", "checkWriteExternalPermission: " +  (res == PackageManager.PERMISSION_DENIED));
        if (res != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},1);
        }
        return (res == PackageManager.PERMISSION_GRANTED);
    }

}

