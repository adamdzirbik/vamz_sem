package com.example.vamz_new;

/**
 * Trieda reprezentuje vytvoreny audio subor
 */
public class Record {

    private String name;

    public Record(String paName) {
        name = (paName.substring(0, paName.length()-4));
    }

    public String getName() {
        return name;
    }

}
