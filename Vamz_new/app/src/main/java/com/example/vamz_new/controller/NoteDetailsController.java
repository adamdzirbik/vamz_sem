package com.example.vamz_new.controller;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vamz_new.model.NoteModel;
import com.example.vamz_new.Note;
import com.example.vamz_new.R;

import java.util.Date;
import java.util.UUID;

/**
 * Trieda zobrazuje detaily Poznamky
 * Umoznuje mazanie a editovanie existujucej poznamky
 */
public class NoteDetailsController extends AppCompatActivity {

    private EditText noteNameEdit;
    private EditText noteContentEdit;

    private Button confirmBtn;

    private UUID noteId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_details);

        noteId = (UUID)getIntent().getSerializableExtra("noteId");

        Note note = NoteModel.getInstance(getApplicationContext()).getNote(noteId);

        noteNameEdit = (EditText) findViewById(R.id.noteName);
        noteContentEdit = (EditText) findViewById(R.id.noteContent);
        confirmBtn = (Button) findViewById(R.id.confirm);

        noteNameEdit.setText(note.getNoteName());
        noteContentEdit.setText(note.getNoteContent());

        confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkFilledInputs(noteNameEdit) && checkFilledInputs(noteContentEdit)) {
                    Intent intent = new Intent();
                    intent.putExtra("action", "update");
                    setResult(RESULT_OK, intent);
                    String date = new Date().toString();
                    NoteModel.getInstance(getApplicationContext()).updateNote(noteId, noteNameEdit.getText().toString(), noteContentEdit.getText().toString(), date);
                    Toast.makeText(NoteDetailsController.this, "Note has been updated", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(NoteDetailsController.this, "Field input is empty", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    /**
     * Pouzije sa vytvorene menu
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.new_note_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * po stlaceni tlacidla v menu sa vymaze Poznamka
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.removeItem:
                Intent intent = new Intent();
                intent.putExtra("action", "delete");
                setResult(RESULT_OK, intent);
                Log.i("this", "onOptionsItemSelected: "  + noteId);
                NoteModel.getInstance(getApplicationContext()).deleteNote(noteId);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    private boolean checkFilledInputs(TextView textView) {
        if(textView.getText().length() == 0) {
            return false;
        }
        return true;
    }

}
