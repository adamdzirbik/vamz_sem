package com.example.vamz_new.controller;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.example.vamz_new.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Permission;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Trieda sa staro o nahratie a ulozenie audio zaznamu
 * použitý zdroj: https://developer.android.com/reference/android/media/MediaRecorder
 */
public class AudioRecordController extends AppCompatActivity {

    private Button record, stop, play;
    private MediaRecorder mediaRecorder;
    private LottieAnimationView animationView;
    private String file;
    private Chronometer time;
    private boolean permission;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_record);

        record = (Button) findViewById(R.id.record);
        stop = (Button) findViewById(R.id.stop);
        play = (Button) findViewById(R.id.play);
        time = (Chronometer) findViewById(R.id.timer);
        animationView = (LottieAnimationView) findViewById(R.id.audioAnimation);

        stop.setEnabled(false);
        play.setEnabled(false);

        /**
         * Skontrolovanie pristupovych prav
         */
        if (!this.checkWriteExternalPermission()) {
            permission = false;
           // finish();
        }
        if (!this.checkMicPermission()) {
            permission = false;
          //  finish();
        }


        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!permission) {
                    Toast.makeText(getApplicationContext(), "Permissions are not granted", Toast.LENGTH_LONG);
                }

                time.start();
                animationView.playAnimation();
                file = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Records/" + this.createRecordName();
                Log.i("this", "onCreate: " + file);

                mediaRecorder = new MediaRecorder();
                mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
                mediaRecorder.setOutputFile(file);

                try {
                    mediaRecorder.prepare();
                    mediaRecorder.start();
                } catch (IllegalStateException ise) {
                    Log.e("this", "onClick: illegal state exception", ise);
                } catch (IOException ioe) {
                    Log.e("this", "onClick: something is wrong", ioe);
                }
                record.setEnabled(false);
                stop.setEnabled(true);

                Toast.makeText(getApplicationContext(), "Recording...", Toast.LENGTH_LONG).show();
            }

            private String createRecordName() {
                SimpleDateFormat sdf = new SimpleDateFormat("MM.dd - HH:mm:ss", Locale.getDefault());
                String recordName = sdf.format(new Date()) + ".3pg";
                Log.i("this", "createRecordName: " + recordName);
                return recordName;
            }

        });

        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                time.stop();
                animationView.cancelAnimation();
                try {
                    mediaRecorder.stop();
                    time.setBase(SystemClock.elapsedRealtime());
                    time.stop();
                    Intent intent = new Intent();
                    intent.putExtra("action", "new_record");
                    setResult(RESULT_OK, intent);
                }catch (RuntimeException ex ) {
                    Log.e("this", "onClick: stop went wrong", ex);
                }
                mediaRecorder.release();
                mediaRecorder = null;
                record.setEnabled(true);
                stop.setEnabled(false);
                play.setEnabled(true);
                Toast.makeText(getApplicationContext(), "Recording finished", Toast.LENGTH_LONG).show();
            }
        });

        /**
         * po stlaceni tlacidla sa prehra nahraty zvuk
         *
         */
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                File newFile = new File(file); // acquire the file from path string
                FileInputStream inputStream = null;
                try {
                    inputStream = new FileInputStream(newFile);
                } catch (FileNotFoundException e) {
                    Log.e("play sound", "onClick: ", e );
                }
                try {
                    mediaPlayer.setDataSource(inputStream.getFD());
                    inputStream.close();
                } catch (IOException e) {
                    Log.e("play sound", "onClick: ", e );
                }
                try {
                    time.setBase(SystemClock.elapsedRealtime());
                    time.stop();

                    Log.i("play sound", "onClick: " + file );
                    mediaPlayer.prepare();
                    mediaPlayer.start();
                    Toast.makeText(getApplicationContext(), "Playing...", Toast.LENGTH_LONG).show();
                } catch ( Exception e ) {
                    Log.e("play sound", "onClick: ", e );
                }

            }
        });
    }

    /**
     * Skontroluj pristupove prava na zapis
     * @return
     */
    private boolean checkWriteExternalPermission()
    {
        String permission = Manifest.permission.READ_EXTERNAL_STORAGE;
        int res = getApplicationContext().checkCallingOrSelfPermission(permission);
        Log.i("this", "checkWriteExternalPermission: " +  (res == PackageManager.PERMISSION_DENIED));
        if (res != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},1);
        }
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    /**
     * Skontroluj pristupove prava na pouzitie mikrofonu
     * @return
     */
    private boolean checkMicPermission()
    {
        String permission = Manifest.permission.RECORD_AUDIO;
        int res = getApplicationContext().checkCallingOrSelfPermission(permission);
        Log.i("this", "checkWriteExternalPermission: " +  (res == PackageManager.PERMISSION_DENIED));
        if (res != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO},1);
        }
        return (res == PackageManager.PERMISSION_GRANTED);
    }

}
