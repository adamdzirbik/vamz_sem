package com.example.vamz_new.controller;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vamz_new.model.NoteModel;
import com.example.vamz_new.Note;
import com.example.vamz_new.R;

import org.w3c.dom.Text;

public class NewNoteController extends AppCompatActivity {

    private EditText noteNameEdit;
    private EditText noteContentEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_note);

        noteNameEdit = (EditText) findViewById(R.id.noteName);
        noteContentEdit = (EditText) findViewById(R.id.noteContent);

        noteNameEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                noteNameEdit.setText("");
            }
        });

        noteContentEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                noteContentEdit.setText("");
            }
        });
    }

    /**
     * Pouzije sa vytvorene menu
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_new_note, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Po stlaceni tlacidla v menu sa prida novy uzivatel do DB
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addNote:
                if (this.checkFilledInputs(noteContentEdit) && checkFilledInputs(noteContentEdit)) {
                    Note note = new Note();
                    note.setNoteName(noteNameEdit.getText().toString());
                    note.setNoteContent(noteContentEdit.getText().toString());
                    NoteModel.getInstance(getApplicationContext()).insertNote(note);
                    Intent intent = new Intent();
                    intent.putExtra("action", "insert");
                    setResult(RESULT_OK, intent);

                    Toast.makeText(NewNoteController.this, "Note has been added", Toast.LENGTH_SHORT).show();

                    noteNameEdit.setText("");
                    noteContentEdit.setText("");
                } else {
                    Toast.makeText(NewNoteController.this, "Text field is empty", Toast.LENGTH_LONG).show();
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    /**
     * Skontroluj ci nie je TextView prazdny
     * @param textView
     * @return
     */
    private boolean checkFilledInputs(TextView textView) {
        if(textView.getText().length() == 0) {
            return false;
        }
        return true;
    }

}
