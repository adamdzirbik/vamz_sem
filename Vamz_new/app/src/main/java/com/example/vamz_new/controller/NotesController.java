package com.example.vamz_new.controller;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.vamz_new.model.NoteModel;
import com.example.vamz_new.Note;
import com.example.vamz_new.R;

import java.util.List;
import java.util.UUID;

/**
 * Trieda sa stara o zobrazenie, editovanie a pridavanie poznamok
 * Zdroj: https://www.youtube.com/watch?v=RyPBRkH5kCQ
 */
public class NotesController extends AppCompatActivity {

    private RecyclerView notes_rw;
    private List<Note> notesArray;
    private NotesAdapter notesAdapter;
    private int itemPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_notes);
        notes_rw = (RecyclerView) findViewById(R.id.notes_rw);

        /**
         * Nacitanie poznamok z DB
         */
        notesArray = NoteModel.getInstance(getApplicationContext()).getNotes();

        notes_rw.setLayoutManager(new LinearLayoutManager(this));

        notesAdapter = new NotesAdapter(notesArray);
        notes_rw.setAdapter(notesAdapter);
    }

    class NotesAdapter extends RecyclerView.Adapter<NoteViewHolder> {
        private  List<Note> notesList;
        public NotesAdapter(List<Note> notes) {
            super();
            this.notesList = notes;
        }

        /**
         * Vrati viewHolder
         * @param parent
         * @param viewType
         * @return
         */
        @NonNull
        @Override
        public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new NoteViewHolder(parent);
        }

        /**
         * vlozi prvok do viewHoldera
         * @param holder
         * @param position
         */
        @Override
        public void onBindViewHolder(@NonNull NoteViewHolder holder, int position) {
            Log.i("this", "onBindViewHolder: "+ position );
            holder.bind(this.notesList.get(position), position);
        }

        /**
         * Vrati pocet poznamok
         * @return
         */
        @Override
        public int getItemCount() {
            return this.notesList.size();
        }

        /**
         * Nastavi novy zoznam Poznamok
         * @param notes
         */
        public void setNotes(List<Note> notes) {
            this.notesList = notes;
        }

    }

    class NoteViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private UUID noteId;
        private TextView noteName;
        private TextView noteContent;
        private int position;

        /**
         * Vlozi prvky do view holdera
         * @param container
         */
        public NoteViewHolder(ViewGroup container) {
            super(LayoutInflater.from(NotesController.this).inflate(R.layout.note_list_item, container, false));
            itemView.setOnClickListener(this);  // Nastavenie onClick metody na cely Item
            noteName = (TextView) itemView.findViewById(R.id.noteName);
            noteContent = (TextView) itemView.findViewById(R.id.noteContent);
        }

        /**
         * Priradi data prvku na jeho pozici
         * @param note
         * @param paPosition
         */
        public void  bind(Note note, int paPosition) {
            position = paPosition;
            Log.i("this", "onBindViewHolder: "+ note.getNoteId() );
            Log.i("this", "onBindViewHolder: "+ note.getNoteName() );
            Log.i("this", "onBindViewHolder: "+ note.getNoteContent() );

            noteId = note.getNoteId();
            noteName.setText(note.getNoteName());
            noteContent.setText(note.getDate().substring(0,19));
        }

        /**
         * Po kliknuti sa spusti "child" aktivita, ktora zobrazi detaily o poznamke,
         * @param v
         */
        @Override
        public void onClick(View v) {
            itemPosition  = position;
            Intent data = new Intent(NotesController.this, NoteDetailsController.class); // komunikuje s aktivity manager
            data.putExtra("noteId", noteId);
            startActivityForResult(data, 1);
        }
    }

    /**
     * Vytvori nove option menu
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.notes_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Po stlaceni polozky v menu sa spusti "child" aktivita
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.newNote:
                Intent intent = new Intent(NotesController.this, NewNoteController.class);
                startActivityForResult(intent, 2);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    /**
     * Spracuje vysledok "child" aktivit
     * Ak sa nieco zmenilo upozorni Adapter a ten aktualizuje obsah
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            notesArray = NoteModel.getInstance(getApplicationContext()).getNotes();
            notesAdapter.setNotes(notesArray);
            if(requestCode == 1 ) {
                if (data != null) {
                    String action = data.getStringExtra("action");
                    if (action.equals("update")) {
                        notesAdapter.notifyItemChanged(itemPosition);
                    } else if (action.equals("delete")) {
                        notesAdapter.notifyItemRemoved(itemPosition);
                    }
                }
            } else if (requestCode == 2) {
                Log.i("this", "MyClass.getView() — get item number " + notesArray.size());
                notesAdapter.notifyItemInserted(notesArray.size());
            }
        }
    }
}

