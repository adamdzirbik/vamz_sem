package com.example.vamz_new.controller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.airbnb.lottie.LottieAnimationView;
import com.example.vamz_new.R;

/**
 * zobrazenie Hlavnej stranky
 * prepojenie medzi poznamkami a audio zaznamami
 */
public class MainScreenController extends AppCompatActivity {

    private LottieAnimationView btnNotes,btnRecords;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);

        btnNotes = (LottieAnimationView) findViewById(R.id.noteManager);
        btnRecords = (LottieAnimationView) findViewById(R.id.recordManager);

        btnNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainScreenController.this, NotesController.class);
                startActivity(intent);
            }
        });

        btnRecords.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainScreenController.this, RecordsController.class);
                startActivity(intent);
            }
        });
    }
}
