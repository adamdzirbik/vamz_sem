package com.example.vamz_new.model;

import android.os.Environment;
import android.util.Log;

import com.example.vamz_new.Record;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Singleton trieda ktora sluzi na pristup k audio nahravkam
 */
public class RecordModel {

    private static RecordModel recordModel;

    private RecordModel() {
    }

    public static RecordModel getInstance() {
        if(recordModel == null) {
            recordModel = new RecordModel();
        }
        return recordModel;
    }

    /**
     * Metoda vrati zoznam Audio zaznamov, ak ziadne neexistuju vrati prazdny zoznam
     * @return
     */
    public List<Record> getRecords() {

        File dir = new File(Environment.getExternalStorageDirectory()+"/Records");
        if (!dir.isDirectory()){
            Log.i("this", "onCreate: vytvoreny priecinok" );
            dir.mkdir();
        }
        String path = Environment.getExternalStorageDirectory().toString() + "/Records/";
        Log.i("this", "Path: " + path);

        List<Record> records = new ArrayList<>();
        File directory = new File(path);
        Log.i("this", "Length: " + directory.length());
        if (directory.length() != 0) {
            File[] files = directory.listFiles();
            if (files != null) {
                Log.i("this", "Size: " + files.length);
                for (int i = 0; i < files.length; i++) {
                    Record record = new Record(files[i].getName());
                    Log.i("this", "Voice exists: " + files[i].exists() + ", can read : " + files[i].canRead() );
                    records.add(record);
                }
            }
        }
        return records;
    }

}
